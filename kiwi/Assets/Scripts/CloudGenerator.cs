﻿using UnityEngine;
using System.Collections;

public class CloudGenerator : MonoBehaviour {
	public GameObject[] cloudPrefabList;
	public float height = 3f;
	private GameObject end;
	private float time = 0f;
	 
	// Use this for initialization
	void Start () {
		end = GameObject.Find("End");
	}
	
	// Update is called once per frame
	void Update () {
		if(time > 3f){
			int rand = Random.Range(0,cloudPrefabList.Length);
			float randomH = Random.Range(end.transform.position.y + 6f, end.transform.position.y + height + 6f);
			Transform.Instantiate(cloudPrefabList[rand], new Vector3(end.transform.position.x - 10f, randomH, end.transform.position.z), new Quaternion());
			time = 0f;
		}
		time += Time.deltaTime;
	}
}
