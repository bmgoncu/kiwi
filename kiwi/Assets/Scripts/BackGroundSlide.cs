﻿using UnityEngine;
using System.Collections;

public class BackGroundSlide : MonoBehaviour {
	public Transform camera;
	public float speed = 0.1f;

	private float oldX;
	// Use this for initialization
	void Start () {
		oldX = camera.transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
		float locX = camera.transform.position.x;

		if(locX != oldX)
		{
			float difference = locX - oldX;
			transform.position = new Vector3(transform.position.x - difference*speed, transform.position.y, transform.position.z);
		}
		oldX = locX;
	}
}
