﻿#pragma strict
public var jumpClear : boolean = false;
var jumpTimer : float = 0;
function Start () {

}

function Update () {
	if(Input.GetButtonDown("Jump")){
		if(jumpClear){
			transform.rigidbody2D.AddForce(Vector2.up * 500);
			jumpClear = false;
			transform.animation.Play("JumpAnim");
		}
		
	}
	if(Input.GetAxis("Horizontal")){
		transform.localPosition.x += Input.GetAxis("Horizontal") * 0.2;
		if(transform.animation.isPlaying){
			transform.animation.Play("JumpAnim");
		}
	}
	
	if(transform.rigidbody2D.velocity.y < 0){
		transform.rigidbody2D.velocity.y -= 0.5;
	}
	
	jumpTimer += Time.deltaTime;
}

function OnCollisionEnter2D(obj : Collision2D){
	if(obj.collider.name == "Platform"){
		jumpClear = true;
	}
	if(obj.collider.name == "RotatingPlatform" && jumpTimer > 1){
		jumpClear = true;
		jumpTimer = 0;
	}
	if(obj.collider.name.Contains("TreadMill") && jumpTimer > 1){
		jumpClear = true;
		jumpTimer = 0;
	}
	if(obj.collider.name.Contains("Bridge") && jumpTimer > 1){
		jumpClear = true;
		jumpTimer = 0;
	}
	if(obj.collider.name.Contains("Button") && jumpTimer > 1){
		jumpClear = true;
		jumpTimer = 0;
	}
	if(obj.collider.name.Contains("Container") && jumpTimer > 1){
		jumpClear = true;
		jumpTimer = 0;
	}
	if(obj.collider.name.Contains("Ramp") && jumpTimer > 1){
		jumpClear = true;
		jumpTimer = 0;
	}
	
	if(obj.collider.name.Contains("degirmen") && jumpTimer > 1){
		jumpClear = true;
		jumpTimer = 0;
	}
	if(obj.collider.name.Contains("Block") && jumpTimer > 1){
		jumpClear = true;
		jumpTimer = 0;
	}
}