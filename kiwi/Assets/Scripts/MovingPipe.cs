﻿using UnityEngine;
using System.Collections;

public class MovingPipe : MonoBehaviour {
	public float fadeSpeed = 0.08f;
	private bool isCharacterInside = false;
	private SpriteRenderer spriteRenderer = null;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(spriteRenderer == null)
			spriteRenderer = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>();

		if(isCharacterInside){
			Color color = spriteRenderer.color;
			color.a -= fadeSpeed;

			if(color.a < 0)
				color.a = 0f;
			spriteRenderer.color = color;
		}
		else{
			Color color = spriteRenderer.color;
			color.a += fadeSpeed;
			
			if(color.a > 1)
				color.a = 1f;
			spriteRenderer.color = color;
		}
	}

	void OnTriggerEnter2D(Collider2D obj){
		if(obj.gameObject.name == "Character"){
			isCharacterInside = true;
		}
	}

	void OnTriggerExit2D(Collider2D obj){
		if(obj.gameObject.name == "Character"){
			isCharacterInside = false;
		}
	}

}
