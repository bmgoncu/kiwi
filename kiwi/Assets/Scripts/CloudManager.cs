﻿using UnityEngine;
using System.Collections;

public class CloudManager : MonoBehaviour {

	public float leftDeadZone = -65f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.x < leftDeadZone)
			Destroy(gameObject);

		transform.position = new Vector3(transform.position.x - 0.05f, transform.position.y, transform.position.z);
	}
}
