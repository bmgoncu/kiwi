﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {
	public float height = 10f;
	public float time = 0.2f;
	private float initialY;
	private float vel = 0f;
	private bool up = true;
	public float speed = 0.01f;

	// Use this for initialization
	void Start () {
		initialY = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		if(up){
			//transform.position = new Vector3(transform.position.x, Mathf.SmoothDamp(transform.position.y, initialY + height, ref vel, time),transform.position.z);
			transform.position = new Vector3(transform.position.x, transform.position.y + speed, transform.position.z);		
		}
		else{
			//transform.position = new Vector3(transform.position.x, Mathf.SmoothDamp(transform.position.y, initialY, ref vel, time),transform.position.z);
			transform.position = new Vector3(transform.position.x, transform.position.y - speed, transform.position.z);		
		}
		//if(vel <= 0.01f)
		//	up = !up;

		if(transform.position.y >= initialY + height)
			up = false;
		else if(transform.position.y <= initialY)
			up = true;
	}
}
