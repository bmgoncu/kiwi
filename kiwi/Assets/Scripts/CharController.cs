﻿using UnityEngine;
using System.Collections;

public class CharController : MonoBehaviour {

	public float maxspeed = 10f;
	private float lastSpeed = 0f;
	public bool jumpClear = false;
	private float jumpTimer = 0;
	private float attackTimer = 0;
	private Animator animation;	
	private bool isDead = false;
	private float dieTimer = 0f;
	public bool powerW = false;
	public int powerJumpCount = 3;

	// Use this for initialization
	void Start () {
		animation = transform.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(isDead){ 
			dieTimer += Time.deltaTime;
			if(dieTimer > 0.1f)
				animation.SetBool("dead", false);
			if(dieTimer > 2f){
				Application.LoadLevel(Application.loadedLevelName);
			}

			return;
		}
		if(transform.rigidbody2D.velocity.x > maxspeed)
			transform.rigidbody2D.velocity = new Vector2(maxspeed, transform.rigidbody2D.velocity.y);

		float speed = transform.rigidbody2D.velocity.magnitude;
		animation.SetFloat("speed", speed);//Mathf.Abs(Input.GetAxisRaw("Horizontal"))

		if(speed <= lastSpeed && ((transform.rigidbody2D.velocity.x < 0 && Input.GetAxisRaw("Horizontal") > 0)
		   		             || (transform.rigidbody2D.velocity.x > 0 && Input.GetAxisRaw("Horizontal") < 0))){
			animation.SetBool("stopping",true);
		}
		else {
			animation.SetBool("stopping",false);
		}

		if(Input.GetButton("Vertical")){
			//transform.rigidbody2D.AddForce(new Vector2(0,maxspeed * Input.GetAxisRaw("Vertical")));
			//Play("deneme");
		}

		if(Input.GetButton("Horizontal")){
			transform.rigidbody2D.AddForce(new Vector2(maxspeed * Input.GetAxisRaw("Horizontal"),0));

			if(Input.GetAxisRaw("Horizontal") < 0){
				transform.eulerAngles = new Vector2(0,180);
			}
			else{
				transform.eulerAngles = new Vector2(0,0);
			}
		}

		if(Input.GetButtonDown("Jump")){
			if(jumpClear){
				transform.rigidbody2D.AddForce(Vector2.up * 500);
				jumpClear = false;
				animation.SetBool("inAir", !jumpClear);
			}

		}

		if(Input.GetButtonDown("PowerJump") && powerW){
			if(jumpClear){
				transform.rigidbody2D.AddForce(Vector2.up * 700);
				jumpClear = false;
				animation.SetBool("inAir", !jumpClear);
			}
		}

		animation.SetBool("tailAttack", false);
		if(Input.GetButtonDown("TailAttack") && attackTimer > 1f){
			animation.SetBool("tailAttack", true);
			attackTimer = 0f;
		}

		if(transform.rigidbody2D.velocity.y < -0.5f){ //Fast fall
			transform.rigidbody2D.velocity = new Vector2(transform.rigidbody2D.velocity.x,transform.rigidbody2D.velocity.y - 0.5f);

		}

		lastSpeed = speed;
		jumpTimer += Time.deltaTime;
		attackTimer += Time.deltaTime;
	}

	void OnCollisionEnter2D(Collision2D obj){
		if(obj.collider.name == "Platform" || obj.collider.name == "BoulderRock" || obj.collider.name == "PipePlatform"){
			jumpClear = true;
			jumpTimer= 0;
			animation.SetBool("inAir", !jumpClear);
		}
		if(obj.collider.name == "MovingPlatform" && jumpTimer > 1){
			jumpClear = true;
			animation.SetBool("inAir", !jumpClear);
			jumpTimer = 0;
		}
		if(obj.collider.name == "RotatingPlatform" && jumpTimer > 1){
			jumpClear = true;
			animation.SetBool("inAir", !jumpClear);
			jumpTimer = 0;
		}
		if(obj.collider.name.Contains("degirmen") && jumpTimer > 1){
			jumpClear = true;
			animation.SetBool("inAir", !jumpClear);
			jumpTimer = 0;
		}
		if(obj.collider.name.Contains("FallChecker"))
			die();
		if(obj.collider.name.Contains("Spike"))
			die();
		if(obj.collider.name.Contains("Block")){
			jumpClear = true;
			animation.SetBool("inAir", !jumpClear);
			jumpTimer = 0;
		}
		if(obj.collider.name.Contains("Bridge") || obj.collider.name.Contains("Ramp") || obj.collider.name.Contains("Container"))
		{
			jumpClear = true;
			animation.SetBool("inAir", !jumpClear);
			jumpTimer = 0;
		}
		if(obj.collider.name.Contains("Blank")){
			jumpClear = true;
			animation.SetBool("inAir", !jumpClear);
			jumpTimer = 0;
		}
	}

	void OnTriggerEnter2D(Collider2D obj){
		if(obj.gameObject.name.Contains("Spike")){
			die();
		}

		if(obj.gameObject.name.Contains("PowerWActivator"))
		{
			powerW = true;
		}
	}

	private void die(){
		animation.SetBool("dead", true);
		isDead = true;
	}
}
