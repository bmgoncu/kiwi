﻿#pragma strict
var initialPosition : Vector3;

function Start () {
	initialPosition = transform.position;
}

function Update () {
	transform.position = initialPosition;
}