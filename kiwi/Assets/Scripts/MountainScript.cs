﻿using UnityEngine;
using System.Collections;

public class MountainScript : MonoBehaviour {

	public float speed = 0.001f;
	public Transform camera;
	private Vector3 pos;
	private float initPosX;
	private float initPosY;
	private float relativePos = 0f;
	private float vel;
	// Use this for initialization
	void Start () {
		pos = camera.transform.position;
		initPosX = pos.x;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		pos = camera.transform.position;

		relativePos = (initPosX - pos.x) * 0.01f;
		//float newX = Mathf.SmoothDamp(transform.position.x, camera.transform.position.x + relativePos, ref vel, 0.1f);
		//transform.position = new Vector3(newX, transform.position.y, transform.position.z);

		transform.position = new Vector3(camera.transform.position.x + relativePos, transform.position.y, transform.position.z);
	}
}
