﻿#pragma strict
public var blockHealth : int = 3;
public var blockSprites : Sprite[];
public var explosionPrefab : GameObject;
public var destroyByTimer : boolean;
public var destroyTime : float = 2.0;
private var isTrigged : boolean = false;
function Start () {
	if(!destroyByTimer)
		destroyTime = 0f;
}

function Update () {

	if(isTrigged && destroyByTimer){
		if(destroyTime < 0){
			GameObject.Instantiate(explosionPrefab,transform.position,transform.rotation);
			Destroy(this.gameObject);
		}
		destroyTime -= Time.deltaTime;
	
	}
}
function OnCollisionStay2D(obj : Collision2D){
	if(!destroyByTimer){
	
		if(Input.GetButtonUp("TailAttack")){
			if(blockHealth > 1){
				blockHealth--;
				gameObject.GetComponent(SpriteRenderer).sprite = blockSprites[blockHealth-1];
			}else if(destroyTime > 1f + 0.8f){
				//Explode,crumble block animation
				GameObject.Instantiate(explosionPrefab,transform.position,transform.rotation);
				Destroy(this.gameObject);
			}
			
		}
		destroyTime += Time.deltaTime;
	}else{
		isTrigged = true;
	/*
		if(destroyTime < 0){
			GameObject.Instantiate(explosionPrefab,transform.position,transform.rotation);
			Destroy(this.gameObject);
		}
		destroyTime -= Time.deltaTime;*/
	}
}