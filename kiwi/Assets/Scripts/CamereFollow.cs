﻿using UnityEngine;
using System.Collections;

public class CamereFollow : MonoBehaviour {

	public Transform target;
	public float yPositionDif = 5;
	private float leftPos;
	private float rightPos;

	// Use this for initialization
	void Start () {
		leftPos = transform.position.x;
		rightPos = GameObject.Find("End").transform.position.x - 8f;
	}
	
	// Update is called once per frame
	void Update () {
		//transform.position = new Vector3(target.position.x, target.position.y + yPositionDif, transform.position.z);
		transform.position = new Vector3(target.position.x + 4f, transform.position.y , transform.position.z);

		if(transform.position.x < leftPos)//GameObject.Find("Platform").transform)
			transform.position = new Vector3(leftPos, transform.position.y, transform.position.z); //transform.camera.orthographicSize*2
		if(transform.position.x > rightPos)
			transform.position = new Vector3(rightPos, transform.position.y, transform.position.z);
	}
}
