﻿using UnityEngine;
using System.Collections;

public class CoinChecker : MonoBehaviour {
	private GameObject scoreboard;
	public int coinCount = 0;
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		scoreboard = GameObject.Find("ScoreBoard");
		audio = transform.GetComponent<AudioSource>();
		if(PlayerPrefs.HasKey("coinCount")){
			coinCount = PlayerPrefs.GetInt("coinCount");
			scoreboard.guiText.text = "SCORE: " + coinCount;
		}
	}

	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D coll){
		if(coll.gameObject.name.Contains("gold-coin")){
			coinCount++;
			audio.Play();
			scoreboard.guiText.text = "SCORE: " + coinCount;
			GameObject.Destroy(coll.gameObject);
		}
	}
}
