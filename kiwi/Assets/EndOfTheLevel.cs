﻿using UnityEngine;
using System.Collections;

public class EndOfTheLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if(Application.loadedLevelName == "scene")
			PlayerPrefs.DeleteAll();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col){
		if(Application.loadedLevelName == "scene"){
			PlayerPrefs.SetInt("coinCount", GameObject.Find("Character").GetComponent<CoinChecker>().coinCount);
			Application.LoadLevel("scene2");
		}
		else if(Application.loadedLevelName == "scene2"){
			PlayerPrefs.SetInt("coinCount", GameObject.Find("Character").GetComponent<CoinChecker>().coinCount);
			Application.LoadLevel("scene3");
		}
		else if(Application.loadedLevelName == "scene3"){
			PlayerPrefs.SetInt("coinCount", GameObject.Find("Character").GetComponent<CoinChecker>().coinCount);
			Application.LoadLevel("Ending");
		}
	}
}
